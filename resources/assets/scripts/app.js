import 'idempotent-babel-polyfill';
import Vue from 'vue';
import axios from 'axios';

window.axios = axios;
window.axios.defaults.baseURL = SPP_Data.root;
window.axios.defaults.headers.common['X-WP-Nonce'] = SPP_Data.nonce;

import App from './Polls/App.vue';

const app = new Vue( {
    el: '#spp_app',
    components: { App },
    render: h => h( App ),
} );
