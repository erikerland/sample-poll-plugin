<?php
/**
 * Plugin Name: Sample Poll Plugin
 * Description: Sample Poll
 * Version: 1.0.0
 * Author: Erik
 */

defined( 'WPINC' ) || exit;

// @TODO Overloaded main file, move into a more logically organized format

// @TODO pull from data source
function spp_get_polls() {
    return array(
        1 => array(
            'title'   => 'Favorite JavaScript Framework',
            'options' => array( // @TODO helper to dynamically get the count for each
                array(
                    'name'  => 'Vue.js',
                    'count' => 29,
                ),
                array(
                    'name'  => 'React',
                    'count' => 11,
                ),
                array(
                    'name'  => 'Angular',
                    'count' => 8,
                ),
                array(
                    'name'  => 'Backbone.js',
                    'count' => 1,
                ),
                array(
                    'name'  => 'Alpine.js',
                    'count' => 2,
                ),
            ),
            'count' => 51, // @TODO dynamic count
            'voted' => false, // @TODO way to determine if user has voted
        ),
    );
}

//
function spp_get_poll( $data ) {
    $polls = spp_get_polls();

    if ( empty( $polls[ $data['id'] ] ) ) {
        return null;
    }

    return $polls[ $data['id'] ];
}

// @TODO Move to rest controllers
function spp_add_rest_endpoints() {
    $version   = '1';
    $namespace = 'sample-poll-plugin/v' . $version;

    register_rest_route( $namespace, '/polls' . '/(?P<id>[\d]+)', array(
        'methods'  => WP_REST_Server::READABLE,
        'callback' => 'spp_get_poll',
        'args'     => array(
            'id' => array(
                'validate_callback' => function( $param, $request, $key ) {
                    return is_numeric( $param );
                }
            ),
        ),
  ) );
}
add_action( 'rest_api_init', 'spp_add_rest_endpoints' );

//
function spp_enqueue_scripts() {
    wp_register_script( 'spp-app', plugins_url( '/assets/scripts/app.js', __FILE__ ), array(), '1.0.0', true );
    wp_register_style( 'spp-app', plugins_url( '/assets/styles/app.css', __FILE__ ), array(), '1.0.0' );

    $wp_data = array(
        'root'      => esc_url_raw( rest_url( 'sample-poll-plugin/v1/' ) ),
        'nonce'     => wp_create_nonce( 'wp_rest' ),
        'endpoints' => array(
            'polls' => array(
                'get'  => 'polls/',
            ),
        ),
    );

    wp_localize_script( 'spp-app', 'SPP_Data', $wp_data );
}
add_action( 'wp_enqueue_scripts', 'spp_enqueue_scripts' );

// @TODO Accept poll ID from shortcode to pick which poll to display
function spp_shortcode( $atts ) {
    wp_enqueue_script( 'spp-app' );
    wp_enqueue_style( 'spp-app' );
    return '<div id="spp_app"></div>';
}
add_shortcode( 'spp_poll', 'spp_shortcode' );
