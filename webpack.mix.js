let mix = require( 'laravel-mix' );

mix.setPublicPath( 'assets' );

mix.options( {
    processCssUrls: false,
} );

mix.js( 'resources/assets/scripts/app.js', 'scripts' );
mix.sass( 'resources/assets/styles/app.scss', 'styles' );

// General Settings
mix.disableNotifications();
